package dut.flatcraft.ui;

public interface Inventoriable {

	void addTo(Inventory inventory);
}
