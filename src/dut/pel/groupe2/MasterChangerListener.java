package dut.pel.groupe2;

public class MasterChangerListener extends VolumeChangerListener {

    @Override
    public void changeVolume(float value) {
        audio.setMasterVolume(value);
    }
}
