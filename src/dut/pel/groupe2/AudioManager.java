package dut.pel.groupe2;

import java.awt.Point;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeListener;

import dut.flatcraft.Main;
import dut.flatcraft.MineUtils;
import paulscode.sound.SoundSystem;
import paulscode.sound.SoundSystemConfig;
import paulscode.sound.SoundSystemException;
import paulscode.sound.codecs.CodecJOrbis;
import paulscode.sound.libraries.LibraryLWJGLOpenAL;

public class AudioManager {

	// all the effects found from fillRulesFromFile
	private static final Map<String, String> RULES = new HashMap<>();

	static {
		MineUtils.fillRulesFromFile("/soundrules.txt", RULES);
	}

	// library for the sound
	private final SoundSystem mySoundSystem;

	private static AudioManager INSTANCE = new AudioManager();

	// window for the UI of the AudioManager
	private static JDialog window = new JDialog(Main.getFrame(), "Sound Manager");
	private static JPanel panel = new JPanel();

	// Constants used for the volume, used if you want to put all the values to
	// default
	public static final float DEFAULT_VOLUME = 0.25f;
	public static final int DEFAULT_VOLUME_PERCENT = (int) (DEFAULT_VOLUME * 100);

	private static float currentFXVolume = DEFAULT_VOLUME_PERCENT;

	// Create an unique instance of AudioManager and starts background music
	private AudioManager() {
		new NativesExtract(); // used to be available on all devices, the return value is not necessary
		System.setProperty("org.lwjgl.librarypath", new File("natives").getAbsolutePath());

		// set up SoundSystem
		try {
			SoundSystemConfig.addLibrary(LibraryLWJGLOpenAL.class);
			SoundSystemConfig.setCodec("ogg", CodecJOrbis.class);
		} catch (SoundSystemException e) {
			System.err.println("error linking with the plug-ins");
		}
		mySoundSystem = new SoundSystem();

		playBackground();
	}

	public static AudioManager getInstance() {
		return INSTANCE;
	}

	/**
	 * Play an effect for a short amount of time If the file is not found, it will
	 * play stone.ogg by default
	 *
	 * @param rule name of the effect (ex : stone or dirt)
	 */
	public void playEffect(String rule) {
		String filename = RULES.getOrDefault(rule, RULES.get("default"));

		// create source for FX
		mySoundSystem.newSource(false, "FX", "effects/" + filename, false, 0, 0, 0,
				SoundSystemConfig.ATTENUATION_ROLLOFF, SoundSystemConfig.getDefaultRolloff());

		mySoundSystem.setVolume("FX", currentFXVolume);
		mySoundSystem.play("FX");
	}

	/**
	 * Change volume of all music
	 *
	 * @param volume the value of the sound between 0 and 1
	 */
	public void setMasterVolume(float volume) {
		mySoundSystem.setMasterVolume(volume);
	}

	/**
	 * Change volume of the background music useless for now, but maybe in a future
	 * version
	 *
	 * @param volume the value of the sound between 0 and 1
	 */
	public void setBackgroundVolume(float volume) {
		mySoundSystem.setVolume("bg", volume);
	}

	public void setFXVolume(float volume) {
		currentFXVolume = volume;
	}

	private String getLocalElement(String type, String filename) {
		return type + "/" + filename;
	}

	/**
	 * function that put all the background music in a queue
	 */
	private void playBackground() {
		mySoundSystem.backgroundMusic("bg", getLocalElement("musics", "abacab.ogg"), true);
		mySoundSystem.queueSound("bg", getLocalElement("musics", "golden_brown.ogg"));
		mySoundSystem.queueSound("bg", getLocalElement("musics", "never_let_me_down.ogg"));
		mySoundSystem.queueSound("bg", getLocalElement("musics", "midnight_man.ogg"));
		setMasterVolume(DEFAULT_VOLUME);
		setBackgroundVolume(0.40f);
	}

	/**
	 * Create the button that display the menu
	 *
	 * @return le bouton qui ramène au menu
	 */
	public static JButton createButton() {
		JButton button = new JButton("Volume");
		button.setToolTipText("Manages the game sounds");
		button.setFocusable(false);

		JSlider sliderMaster = createSlider("Volume générale :", new MasterChangerListener());
		panel.add(sliderMaster);

		JSlider sliderBg = createSlider("Volume de fond : ", new BackgroundChangerListener());
		panel.add(sliderBg);

		JSlider sliderFX = createSlider("Volume des effets : ", new EffectChangerListener());
		panel.add(sliderFX);

		window.add(panel);
		window.setResizable(false);
		window.pack();

		button.addActionListener(p -> AudioManager.positionVolume(button, window));

		return button;
	}

	private static void positionVolume(JButton button, JDialog window) {
		if (window.isVisible()) {
			window.setVisible(false);
		} else {
			Point pos = button.getLocation();
			window.setLocation(pos.x, Main.getFrame().getHeight() - 70 - window.getHeight());
			window.setVisible(true);
		}
	}

	private static JSlider createSlider(String label, ChangeListener listener) {
		JSlider slider = new JSlider(0, 100, AudioManager.DEFAULT_VOLUME_PERCENT);
		slider.addChangeListener(listener);
		slider.setFocusable(false);
		panel.add(new JLabel(label));

		return slider;
	}

}
