package dut.pel.groupe2;

public class BackgroundChangerListener extends VolumeChangerListener {

    @Override
    public void changeVolume(float value) {
        audio.setBackgroundVolume(value);
    }
}
