package dut.pel.groupe2;

public class EffectChangerListener extends VolumeChangerListener {
    @Override
    public void changeVolume(float value) {
        audio.setFXVolume(value);
    }
}
