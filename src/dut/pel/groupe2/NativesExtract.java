package dut.pel.groupe2;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class NativesExtract {
	private static final int BUFFER_SIZE = 4096;

	// On extrait

	public NativesExtract() {
		String os = System.getProperty("os.name").toLowerCase();

		if (os.contains("win")) {
			unzip("lwjgl-win.jar");
		} else if (os.contains("mac")) {
			unzip("lwjgl-osx.jar");
		} else {
			unzip("lwjgl-lnx.jar");
		}
	}

	private InputStream getJarAsInputStream(String name) {
		return getClass().getResourceAsStream("/natives/" + name);
	}

	public void unzip(String jarFile) {
		File destDir = new File("natives");
		if (!destDir.exists()) {
			destDir.mkdir();
		}
		try {
			ZipInputStream zipIn = new ZipInputStream(getJarAsInputStream(jarFile));
			ZipEntry entry = zipIn.getNextEntry();
			// iterates over entries in the zip file
			while (entry != null) {
				String filePath = "natives" + File.separator + entry.getName();
				if (!entry.isDirectory()) {
					// if the entry is a file, extracts it
					extractFile(zipIn, filePath);
				} else {
					// if the entry is a directory, make the directory
					File dir = new File(filePath);
					dir.mkdir();
				}
				zipIn.closeEntry();
				entry = zipIn.getNextEntry();
			}
			zipIn.close();
		} catch (Exception e) {
			System.err.println("Impossible d'extraire les natives. Fin du programme");
			System.exit(1);
		}
	}

	/**
	 * Extracts a zip entry (file entry)
	 * 
	 * @param zipIn
	 * @param filePath
	 * @throws IOException
	 */
	private void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
		BufferedOutputStream bos = null;
		try {
			bos = new BufferedOutputStream(new FileOutputStream(filePath));

			byte[] bytesIn = new byte[BUFFER_SIZE];
			int read = 0;
			while ((read = zipIn.read(bytesIn)) != -1) {
				bos.write(bytesIn, 0, read);
			}

		} catch (FileNotFoundException e) {
			System.err.println("Impossible d'extraire : Une erreur est survenue");
			System.exit(1);
		} finally {
			if (bos != null)
				bos.close();
		}
	}
}
