package dut.pel.groupe2;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public abstract class VolumeChangerListener implements ChangeListener {

    AudioManager audio = AudioManager.getInstance();

    @Override
    public void stateChanged(ChangeEvent e) {
        JSlider source = (JSlider) e.getSource();
        this.changeVolume((float)source.getValue() / 100);
    }

    public abstract void changeVolume(float value);
}
