#!/bin/bash
set -e
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8

find src -name "*.java" -print >javafiles
if [ ! -d bin ]; then
    mkdir bin
fi
javac -d bin -cp "lib/dpprocessor.jar:lib/codecjorbis-20101023.jar:lib/codecwav-20101023.jar:lib/libraryjavasound-20101123.jar:lib/librarylwjglopenal-20100824.jar:lib/lwjgl_util-2.9.4-nightly-20150209.jar:lib/lwjgl-2.9.4-nightly-20150209.jar:lib/soundsystem-20120107.jar" -source 8 -target 8 @javafiles
cp -R src/natives bin
cp -R src/textures bin
cp -R src/Sounds bin
for j in lib/*.jar; do unzip -o $j -d bin/; done
cp src/*.txt bin
jar -cfm flatcraft.jar manifest.mf -C bin/ .
